<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 3/13/18
 * Time: 12:46 PM
 */

class City_model extends CI_Model
{

    public function isAvailable( $val ){
        
        $result_id = 0;
        $sql = "SELECT City_id FROM tbl_city WHERE City_Name =\"".$val."\"";
        
        $query = $this->db->query($sql);

        $result = $query->result();
        $result_id = (count($result) > 0)?$result[0]->City_id:0;
        
        return $result_id;
        
    }
    
    public function getCityName( $city_id ){
        
        $sql = "SELECT City_Name FROM tbl_city WHERE City_id =\"".$city_id."\"";
        
        $query = $this->db->query($sql);

        $result = $query->result();
        
        return $result[0]->City_Name;
    }
    
    public function getAll( $filters ){

        $where = array();

        // can be array fo devices
        $device_id = isset( $filters['devices_id'] ) ? $filters['devices_id'] : 0;
        $country_id = isset( $filters['country_id'] ) ? (int)$filters['country_id'] : 0;

        if( is_array( $device_id ) && count( $device_id ) > 0 ){
            $where[] = 'tdi.Device_Id IN ('. implode(',', $device_id ) .')';

        }
        elseif( $device_id > 0 ){
            $where[] = 'tdi.Device_Id = '. $device_id;

        }

        if( $country_id > 0 ){
            $where[] = 'tc.Country_Id = '. $country_id;
        }

        $where = implode(' AND ', $where );
        if( $where ){
            $where = "WHERE $where";
        }



        $sql = "SELECT DISTINCT c.City_Id, c.Country_Id, c.City_Name, c.City_Latitude, c.City_Longitude, c.City_Zoom, c.city_centre_lat, c.city_centre_long, c.marker_type FROM tbl_city AS c INNER JOIN tbl_clinics AS tc ON tc.City_Id = c.City_Id INNER JOIN tbl_device_install AS tdi ON tdi.Clinic_Id = tc.Clinic_Id $where";

        $query = $this->db->query( $sql );

        return $query->result();


        if( isset($filters['devices_id']) && $filters['devices_id'] > 0 ){
            $this->db->join('tbl_map_report', 'tbl_map_report.City_Id = tbl_city.City_Id');

            if( is_array( $filters['devices_id']  ) && count( $filters['devices_id']  ) > 0 ){
                // foreach( $filters['devices_id'] as $device ){
                //     $this->db->where('tbl_map_report.Device_Id', $device );
                // }
                $this->db->where_in('tbl_map_report.Device_Id', $filters['devices_id'] );
            }
            elseif( $filters['devices_id']  > 0 ){
                $this->db->where('tbl_map_report.Device_Id', $filters['devices_id'] );
            }

        }

        if( isset($filters['country_id']) && $filters['country_id'] > 0 ){
            $this->db->where('tbl_city.Country_Id', $filters['country_id'] );
        }

        $query = $this->db->get('tbl_city');

        return $query->result();
    }
	
	public function getAllCities(){

        $sql = "SELECT c.City_Id, c.City_Name FROM tbl_city AS c";
        $query = $this->db->query( $sql );
		$result = $query->result();
		
		$city_arr = array();
		foreach($result as $val_city) {
			$city_arr[strtolower($val_city->City_Name)] = $val_city->City_Id;
		}

        return $city_arr;
    }
   
	public function isCityExist($city_name, $all_cities) {
		$city_id = 0;
		$city_name = strtolower($city_name);
        if (array_key_exists($city_name, $all_cities)) {
			$city_id = $all_cities[$city_name];
		}
        
        return $city_id;
        
   }
}