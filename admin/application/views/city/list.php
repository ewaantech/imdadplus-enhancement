<?php 
$this->load->view('header');
?>
<script type="text/javascript">
    $(document).ready(function() {
           $('.alert-success').show().fadeOut(2000);        
    });
</script>
                     <!-- page content -->
                     <div class="right_col" role="main">
          <div class="">
<!--            <div class="page-title">
              <div class="title_left">
                <h3>Tables <small>Some examples to get you started</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>-->

            <div class="clearfix"></div>

            <div class="row">

            <?php if ($this->session->flashdata('message')) { ?> 
            <div class="alert alert-success">
              <strong><?php echo $this->session->flashdata('message'); ?></strong>
            </div>
            <?php } ?> 

              <div class="col-md-12 col-sm-12 col-xs-12">
               <form id="addcity" action="create" enctype="multipart/form-data" method="post" accept-charset="utf-8"></form>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>City Listing</h2>
                    <button class="btn btn-primary" style="float: right"type="button" onclick="addcity()">Add City</button>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                      <form id="editcity" action="edit" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                          <input type="hidden" id="editcityhidden" name="editcityhidden" value="">
                          
                      </form>
                      <form id="deletecity" action="delete" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                          <input type="hidden" id="deletecityhidden" name="deletecityhidden" value="">
                          <input type="hidden" id="deletecitystatus" name="deletecitystatus" value="">
                      </form>
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
<!--                            <th>
                              <input type="checkbox" id="check-all" class="flat">
                            </th>-->
                            <th class="column-title">Sl </th>
                            <th class="column-title">Country Name </th>
                            <th class="column-title">City Name </th>
                            <th class="column-title">Nearest City </th>
                            <th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                            <?php 
                            $i = 0;
                            foreach($cities as $datas){ 
                            $i++;
                            ?>
                            <tr class="even pointer">
                            <td class=" "><?php echo $i; ?></td>
                              <td class=" "><?php echo $datas['countryname']; ?></td>
                              <td class=" "><?php echo $datas['cityname']; ?></td>
                              <td class=" "><?php echo $datas['nearest_city_name']; ?></td>
                                <td class=" last">
                              <a href="<?php  echo base_url() ?>city/edit/<?php echo $datas['cityid']; ?>" class="glyphicon glyphicon-edit" style="float: left;font-size:24px;margin-right: 10px;color: #337ab7;cursor: pointer;" value="cityedit"></a>
<!--                              <span class="glyphicon glyphicon-remove" style="float: left;font-size:24px;margin-right: 10px;color: red;cursor: pointer;" onclick="citydelete(<?php echo $datas['cityid']; ?>)" value="cityedit"></span>    -->
                              </td>

                            </tr>
                        <?php } ?>
                        </tbody>
                      </table>
                    </div>
							
						
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<script type="text/javascript">
    
   function addcity(){
        var url = "<?php  echo base_url() ?>city/create";
        $(location).attr('href',url);
    }
    function cityedit(id){
        var url = "<?php  echo base_url() ?>city/edit/"+id;
        $(location).attr('href',url);
    }
    function citydelete(id){
        var url = "<?php  echo base_url() ?>city/delete/"+id;
        $(location).attr('href',url);
    }
    function harddelete(id){
        var url = "<?php  echo base_url() ?>city/harddelete/"+id;
        $(location).attr('href',url);
    }
</script>
<?php 
$this->load->view('footer');
?>
