<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * User Management class created by CodexWorld
 */
class City extends MY_Controller {

    function __construct() {
        $this->data = parent::__construct();

        $this->load->library('form_validation');
    }

    /*
     * Position listing
     */

    public function index() {


        $cityResult['cities'] = $this->Cities->cityList();
        //echo '<pre>'; print_r($cityResult); exit;
        $this->data = array_merge($this->data, $cityResult);

        $this->load->view('city/list', $this->data);
    }

    /* Save or create form city details */

    public function create() {
        $cityResult['countries'] = $this->Cities->countryList();
        $cityResult['cities'] = $this->Cities->cityList();
        

        $this->data = array_merge($this->data, $cityResult);
        $this->load->view('city/create', $this->data);
    }

    /* Save or create city details */

    public function savecity() {
        $cityResult = array();
        $cityResults = array();

        $name = $this->input->post('name');
        $country = $this->input->post('countries');
        $latitude_listing = $this->input->post('latitude_listing');
        $longitude_listing = $this->input->post('longitude_listing');
        $latitude_page = $this->input->post('latitude_page');
        $longitude_page = $this->input->post('longitude_page');
        $sitecore_lat = $this->input->post('sitecore_lat');
        $sitecore_lng = $this->input->post('sitecore_lng');
        $sitecore_zoom = $this->input->post('sitecore_zoom');
        $zoom = $this->input->post('zoom');
        $marker_type = $this->input->post('marker_type');
        $nearest_city = $this->input->post('nearest_city');
        
        $density_point_1_lat = 0;
        $density_point_1_lng = 0;
        $set_high_density = 0;
        $density_point_2_lat = 0;
        $density_point_2_lng = 0;
        $set_medium_density = 0;        
        $density_point_3_lat = 0;
        $density_point_3_lng = 0;
       // $set_low_density = $this->input->post('set_low_density');
       
        $cityResults['details'] = array('Country_Id' => $country,
            // 'city' => $this->input->post('city'),
            'City_Name' => $name,
            'City_Latitude' => $latitude_listing,
            'City_Longitude' => $longitude_listing,
            'city_centre_lat' => $latitude_page,
            'city_centre_long' => $longitude_page,
            'sitecore_lat' => $sitecore_lat,
            'sitecore_lng' => $sitecore_lng,
            'sitecore_zoom' => $sitecore_zoom,
            'zoom' => $zoom,
            'nearest_city' => $nearest_city,
            'marker_type' => $marker_type,
            'density_point_1_lat'=> $latitude_listing,
            'density_point_1_lng'=> $longitude_listing,
            'set_low_density'=> 'YES',
            'density_point_2_lat'=> $latitude_listing,
            'density_point_2_lng'=> $longitude_listing,
            'set_medium_density'=> 'YES',
            'density_point_3_lat'=> $latitude_listing,
            'density_point_3_lng'=> $longitude_listing,
            'set_low_density'=> 'YES'            
            );
        
//echo '<pre>'; print_r($cityResults); exit;
        $this->Cities->insertCity($cityResults);

        $this->session->set_flashdata('message', 'Created City Successfully');

        // $this->load->view('company/list', $this->data);
        redirect(base_url() . 'city/');
    }

    /* Edit city details */

    public function edit() {

        $city_id = $this->uri->segment(3);
        $cityResult['citys'] = $this->Cities->getCity($city_id);
        $cityResult['countries'] = $this->Cities->countryList();
        $cityResult['cities'] = $this->Cities->cityList();


        $cityResult['details']['cityname'] = $cityResult['citys'][0]['City_Name'];
        $cityResult['details']['countryid'] = $cityResult['citys'][0]['Country_Id'];
        $cityResult['details']['lat_listing'] = $cityResult['citys'][0]['City_Latitude'];
        $cityResult['details']['long_listing'] = $cityResult['citys'][0]['City_Longitude'];
        $cityResult['details']['lat_page'] = $cityResult['citys'][0]['city_centre_lat'];
        $cityResult['details']['long_page'] = $cityResult['citys'][0]['city_centre_long'];
        $cityResult['details']['zoom'] = $cityResult['citys'][0]['City_Zoom'];
        $cityResult['details']['sitecore_lat'] = $cityResult['citys'][0]['sitecore_lat'];
        $cityResult['details']['sitecore_lng'] = $cityResult['citys'][0]['sitecore_lng'];
        $cityResult['details']['sitecore_zoom'] = $cityResult['citys'][0]['sitecore_zoom'];
        $cityResult['details']['marker_type'] = $cityResult['citys'][0]['marker_type'];
        $cityResult['details']['nearest_city'] = $cityResult['citys'][0]['Nearest_city_id'];
        $cityResult['details']['density_point_1_lat'] = $cityResult['citys'][0]['density_point_1_lat'];
        $cityResult['details']['density_point_1_lng'] = $cityResult['citys'][0]['density_point_1_lng'];
        $cityResult['details']['set_high_density'] = $cityResult['citys'][0]['set_high_density'];
        $cityResult['details']['density_point_2_lat'] = $cityResult['citys'][0]['density_point_2_lat'];
        $cityResult['details']['density_point_2_lng'] = $cityResult['citys'][0]['density_point_2_lng'];
        $cityResult['details']['set_medium_density'] = $cityResult['citys'][0]['set_medium_density'];
        $cityResult['details']['density_point_3_lat'] = $cityResult['citys'][0]['density_point_3_lat'];
        $cityResult['details']['density_point_3_lng'] = $cityResult['citys'][0]['density_point_3_lng'];
        $cityResult['details']['set_low_density'] = $cityResult['citys'][0]['set_low_density'];
//        $cityResult['details']['high_density'] = $cityResult['citys'][0]['high_density'];
//        $cityResult['details']['medium_density'] = $cityResult['citys'][0]['medium_density'];
//        $cityResult['details']['low_density'] = $cityResult['citys'][0]['low_density'];
        
       $data['json_to_display'] = '[[24.48868256,45.16705263],[24.55207276,54.219787],[29.95639876,47.49615419],[24.50846801,51.17429137],[21.29094899,56.19732606]]';// Ccontry LAT
        
        $data['center'] = '{lat: 23.8859, lng: 45.0792}';
        $data['zoom'] = $cityResult['details']['zoom'];
        if (!empty($city_id)) {
            $markerDetails = $this->Clinics_model->getLatLng($city_id);
            if (!empty($markerDetails)) {
                $data['json_to_display'] = json_encode($markerDetails);
            }
        }
        $data['cityname'] = $cityResult['citys'][0]['City_Name'];
        $countryarr = $this->Cities->getCountry($cityResult['citys'][0]['Country_Id']);
        $data['countryname'] = $countryarr[0]['Country_Name'];        
        $this->data = array_merge($this->data, $data);
        $this->data = array_merge($this->data, $cityResult);
        
      // echo '<pre>'; print_r($this->data); exit;

        $this->load->view('city/edit', $this->data);
    }

    /* Save or create city details */

    public function updatecity() {
       $city_id = $this->input->post('cityid');
        $cityResult = array();
        $cityResults = array();
        
       

        $name = $this->input->post('name');
        $country = $this->input->post('countries');
        $latitude_listing = $this->input->post('latitude_listing');
        $longitude_listing = $this->input->post('longitude_listing');
        $latitude_page = $this->input->post('latitude_page');
        $longitude_page = $this->input->post('longitude_page');
        $zoom = $this->input->post('zoom');
        $sitecore_lat = $this->input->post('sitecore_lat');
        $sitecore_lng = $this->input->post('sitecore_lng');
        $sitecore_zoom = $this->input->post('sitecore_zoom');
        $marker_type = $this->input->post('marker_type');
        $nearest_city = $this->input->post('nearest_city');
        
        $density_point_1_lat = $this->input->post('density_point_1_lat');
        $density_point_1_lng = $this->input->post('density_point_1_lng');
        $set_high_density = $this->input->post('set_high_density');
        $density_point_2_lat = $this->input->post('density_point_2_lat');
        $density_point_2_lng = $this->input->post('density_point_2_lng');
        $set_medium_density = $this->input->post('set_medium_density');        
        $density_point_3_lat = $this->input->post('density_point_3_lat');
        $density_point_3_lng = $this->input->post('density_point_3_lng');
        $set_low_density = $this->input->post('set_low_density');
        
//        $high_density = $this->input->post('high_density');
//        $medium_density = $this->input->post('medium_density');
//        $low_density = $this->input->post('low_density');

       
        $cityResults['details'] = array('Country_Id' => $country,
            'City_Name' => $name,
            'City_Latitude' => $latitude_listing,
            'City_Longitude' => $longitude_listing,
            'city_centre_lat' => $latitude_page,
            'city_centre_long' => $longitude_page,
            'sitecore_lat' => $sitecore_lat,
            'sitecore_lng' => $sitecore_lng,
            'sitecore_zoom' => $sitecore_zoom,
            'zoom' => $zoom,
            'nearest_city' => $nearest_city,
            'marker_type' => $marker_type,
            'density_point_1_lat'=> $density_point_1_lat,
            'density_point_1_lng'=> $density_point_1_lng,
            'set_high_density'=> $set_high_density,
            'density_point_2_lat'=> $density_point_2_lat,
            'density_point_2_lng'=> $density_point_2_lng,
            'set_medium_density'=> $set_medium_density,
            'density_point_3_lat'=> $density_point_3_lat,
            'density_point_3_lng'=> $density_point_3_lng,
            'set_low_density'=> $set_low_density 
//            'high_density'=> $high_density,
//            'medium_density'=> $medium_density,
//            'low_density'=> $low_density,
        );
        
         
        $this->Cities->updateCity($cityResults, $city_id);
        $this->session->set_flashdata('message', 'Updated City Successfully');
        redirect(base_url() . 'city/edit/'.$city_id);
    }

    /* Delete city details */

    public function delete() {
        $cityResult = array();

        $city_id = $this->uri->segment(3);

        $this->Cities->deleteCity($city_id);
        $this->session->set_flashdata('message', "Deleted City Successfully");
        //$this->load->view('city/viewcitys', $this->data);
        redirect(base_url() . 'city/');
    }
    
}
