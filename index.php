<?php
session_start();

include 'config.php';

if( ( isset($_GET['code']) && !empty($_GET['code']) ) || ( isset($_GET['i']) && !empty($_GET['i']) ) ) {
    $_SESSION['logged_in'] = true;
    header("Location: " . SITE_URL);
    exit;
}

//if( !isset($_SESSION['logged_in']) || $_SESSION['logged_in'] != true ) {
//	header("Location: https://login.salesforce.com/services/oauth2/authorize?response_type=code&client_id=3MVG9WtWSKUDG.x4eaHETg6unj71r.ongIx.hU3eQyUWriVRxyXae0B4u4yAGOG0hUlSmNDYupDRhdj_NKKfG&redirect_uri=" . SITE_URL . "/");
//	exit;
//}
//?>
<html ver="1.0">
    <head>
        <title>Imdad + | Aesthetics Optimised</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="css/ionicons.min.css" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/odometer-theme-minimal.css" />
        <link rel="shortcut icon" type="image/png" href="http://www.imdad.com/images/favicon.ico"/>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-3.1.0.min.js" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
	<script>
//            $(document).ready(function() {
//                $('.form-check-label').click(function() {
//                    jQuery(".loading-box").show();
//                    jQuery(".loading-box-overlay").show();
//                });
//            });
        </script
    </head>
    <body>

        <?php

        function slugify($text) {
            // replace non letter or digits by -
            $text = preg_replace('~[^\pL\d]+~u', '-', $text);

            // transliterate
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

            // remove unwanted characters
            $text = preg_replace('~[^-\w]+~', '', $text);

            // trim
            $text = trim($text, '-');

            // remove duplicate -
            $text = preg_replace('~-+~', '-', $text);

            // lowercase
            $text = strtolower($text);

            if (empty($text)) {
                return 'n-a';
            }

            return $text;
        }
        ?>

        <div class="googlemap_info">
            <div class="map_body">

                <div class="side-logo">
                    <img src="img/logo.png">
                </div>
                <div class="new-search">
                    <h3 class="c_name" id="mapview">
                        <span style="display:none;" class="mapview-label">Middle East</span><span class="mapviewshow-label">Middle East</span> <span class="city_count" id="city_count"></span>
                    </h3>
                    <h3 class="d_name" id="filterview">
                        <span class="filterview-label">Selected Devices : </span>&nbsp; <span class="devices_selected" id="devices_selected"></span>
                    </h3>
                    <form autocomplete="off" action="">
                        <div class="autocomplete">
                            <?php 
                            $json = json_decode(file_get_contents(MAIN_SITE_URL . "/json/datacoverage.json"),true);
                            $clinicArray = Array();
                            foreach ($json['clinicslist'] as $clinic) {
                                $clinicArray[] = $clinic['Clinic_Name']; 
                                $clinicArrayCountry[slugify($clinic['Country_Name'])][] = $clinic['Clinic_Name']; 
                                $clinicArrayCity[slugify($clinic['Country_Name'])][slugify($clinic['City_Name'])][] = $clinic['Clinic_Name']; 
                            } 
                            ?>
                            <input name="clinicdata" id="clinicdata" type="hidden" value='<?php echo json_encode($clinicArray); ?>'/> 
                            <input name="clinicdatacountry" id="clinicdatacountry" type="hidden" value='<?php echo json_encode($clinicArrayCountry); ?>'/> 
                            <input name="clinicdatacity" id="clinicdatacity" type="hidden" value='<?php echo json_encode($clinicArrayCity); ?>'/> 
                            <?php foreach ($json['clinicslist'] as $clinic): ?>
                            <input name="clinic_<?php echo $clinic['Clinic_Name']; ?>" id="clinic_<?php echo $clinic['Clinic_Name']; ?>" type="hidden" value="<?php echo $clinic['Clinic_Name_Slugify']; ?>">
                            <input name="country_<?php echo $clinic['Clinic_Name']; ?>" id="country_<?php echo $clinic['Clinic_Name']; ?>" type="hidden" value="<?php echo $clinic['Country_Name']; ?>">
                            <input name="city_<?php echo $clinic['Clinic_Name']; ?>" id="city_<?php echo $clinic['Clinic_Name']; ?>" type="hidden" value="<?php echo $clinic['City_Name']; ?>">
                            <?php endforeach; ?>
                            <div class="ui-widget">
                              <input id="myInput">
                            </div>
                        </div>
                        <a class="reload" href="<?php echo MAIN_SITE_URL; ?>"></a>
                        <a id="goback" class="search" href="#" style="display:none;"></a>
                        <a class="soc-share" href="http://contactme.imdad.com/imdad-dashboard-2018/" target="_blank"></a>
                    </form>
                </div>
                <div id="class_type_title" style="display:none;">
                    <div class="tab-title">
                        <h2>Pin Type</h2>
                    </div>                

                    <div class="tab-block" style="background-color: #fff !important; margin-bottom: 1.75em !important;">
                        <div class="tab-content imdad-tab-content">
                            <div>
                                <div class="list_item_checkbox">                
                                    <div class="form-check form-check-inline" id="classtype_imdadonly" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-classcheck-input" type="checkbox" id="chk_imdadonly" value="Imdad Only">
                                        <label class="form-check-label" for="chk_imdadonly">
                                            Imdad Only
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline" id="classtype_imdadandcompetitor" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-classcheck-input" type="checkbox" id="chk_imdadandcompetitor" value="Imdad and Comptitor">
                                        <label class="form-check-label" for="chk_imdadandcompetitor">
                                            Imdad And Competitor
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline" id="classtype_competitoronly" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-classcheck-input" type="checkbox" id="chk_competitoronly" value="Comptitor Only">
                                        <label class="form-check-label" for="chk_competitoronly">
                                            Competitor Only
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline" id="classtype_none" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-classcheck-input" type="checkbox" id="chk_none" value="None">
                                        <label class="form-check-label" for="chk_none">
                                            None
                                        </label>
                                    </div>

                                </div>
                            </div>                        
                        </div>
                    </div>
                    
                    <div class="tab-title">
                        <h2>Last Visit</h2>
                    </div>                

                    <div class="tab-block" style="background-color: #fff !important; margin-bottom: 1.75em !important;">
                        <div class="tab-content imdad-tab-content">
                            <div>
                                <div class="list_item_checkbox">                
                                    <div class="form-check form-check-inline" id="lastsales_30" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-lastsalescheck-input" type="checkbox" id="lastsaleschk_30" value="30">
                                        <label class="form-check-label" for="lastsaleschk_30">
                                            30 days
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline" id="lastsales_60" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-lastsalescheck-input" type="checkbox" id="lastsaleschk_60" value="60">
                                        <label class="form-check-label" for="lastsaleschk_60">
                                            60 days
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline" id="lastsales_90" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-lastsalescheck-input" type="checkbox" id="lastsaleschk_90" value="90">
                                        <label class="form-check-label" for="lastsaleschk_90">
                                            90 days
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline" id="lastsales_180" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-lastsalescheck-input" type="checkbox" id="lastsaleschk_180" value="180">
                                        <label class="form-check-label" for="lastsaleschk_180">
                                            180 days
                                        </label>
                                    </div>

                                </div>
                            </div>                        
                        </div>
                    </div>                     
                    
                    
                 
 
                    
                    <div class="tab-title">
                        <h2>Last Training</h2>
                    </div>                

                    <div class="tab-block" style="background-color: #fff !important; margin-bottom: 1.75em !important;">
                        <div class="tab-content imdad-tab-content">
                            <div>
                                <div class="list_item_checkbox">                
                                    <div class="form-check form-check-inline" id="lasttraining_30" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-lasttrainingcheck-input" type="checkbox" id="lasttrainingchk_30" value="30">
                                        <label class="form-check-label" for="lasttrainingchk_30">
                                            30 days
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline" id="lasttraining_60" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-lasttrainingcheck-input" type="checkbox" id="lasttrainingchk_60" value="60">
                                        <label class="form-check-label" for="lasttrainingchk_60">
                                            60 days
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline" id="lasttraining_90" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-lasttrainingcheck-input" type="checkbox" id="lasttrainingchk_90" value="90">
                                        <label class="form-check-label" for="lasttrainingchk_90">
                                            90 days
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline" id="lasttraining_180" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-lasttrainingcheck-input" type="checkbox" id="lasttrainingchk_180" value="180">
                                        <label class="form-check-label" for="lasttrainingchk_180">
                                            180 days
                                        </label>
                                    </div>

                                </div>
                            </div>                        
                        </div>
                    </div> 
                    
                    
                    <div class="tab-title">
                        <h2>Last Equipment Installation</h2>
                    </div>                

                    <div class="tab-block" style="background-color: #fff !important; margin-bottom: 1.75em !important;">
                        <div class="tab-content imdad-tab-content">
                            <div>
                                <div class="list_item_checkbox">                
                                    <div class="form-check form-check-inline" id="eqpmt_30" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-eqpmtcheck-input" type="checkbox" id="eqpmtchk_30" value="30">
                                        <label class="form-check-label" for="eqpmtchk_30">
                                            30 days
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline" id="eqpmt_60" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-eqpmtcheck-input" type="checkbox" id="eqpmtchk_60" value="60">
                                        <label class="form-check-label" for="eqpmtchk_60">
                                            60 days
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline" id="eqpmt_90" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-eqpmtcheck-input" type="checkbox" id="eqpmtchk_90" value="90">
                                        <label class="form-check-label" for="eqpmtchk_90">
                                            90 days
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline" id="eqpmt_180" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-eqpmtcheck-input" type="checkbox" id="eqpmtchk_180" value="180">
                                        <label class="form-check-label" for="eqpmtchk_180">
                                            180 days
                                        </label>
                                    </div>

                                </div>
                            </div>                        
                        </div>
                    </div>
                    
                    
                    <div class="tab-title">
                        <h2>Open Opportunity</h2>
                    </div>                

                    <div class="tab-block" style="background-color: #fff !important; margin-bottom: 1.75em !important;">
                        <div class="tab-content imdad-tab-content">
                            <div>
                                <div class="list_item_checkbox">                
                                    <div class="form-check form-check-inline" id="openopportunity_yes" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-openopportunitycheck-input" type="checkbox" id="openopportunitychk_yes" value="1">
                                        <label class="form-check-label" for="openopportunitychk_yes">
                                            Yes
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline" id="openopportunity_no" style="margin-left: 0.25em !important; width: 47.5% !important;">
                                        <input class="form-openopportunitycheck-input" type="checkbox" id="openopportunitychk_no" value="0">
                                        <label class="form-check-label" for="openopportunitychk_no">
                                            No
                                        </label>
                                    </div>

									</div>
                            </div>                        
                        </div>
                    </div> 
 
                    <div class="tab-title">
                        <h2>Total Clinics : <span id="clinic_total_count"></span></h2>
                    </div>  
                    
                                        
                    
                </div>


                <div class="information_block"  style="display:none;">
                    <div id="map-accordion" role="tablist" aria-multiselectable="true" class="map_accordian">

						<!-- device install count all countries except kuwait -->
                        <div class="card clinicblue" style="display:none">
                            <a data-toggle="collapse" href="#taboneclinic" aria-expanded="true" aria-controls="taboneclinic" class="title-wrapper collapsed">
                                <div class="card-header" role="tab">
                                    <h3>EXPERIENCE :  SHARED</h3>
                                    <h4><img src="img/imdad.knowhow.png" alt="imdad knowhow"></h4>
                                </div>
                            </a>

                            <div id="taboneclinic" class="collapse show" role="tabpanel" aria-expanded="true">
                                <div class="card-block">
                                    <div class="row no-gutters rep-clinic-row">
                                        <div class="col-lg-6 card-item" id="gentle-laster">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                   <span id="gentle_laster" class="rep-span-device"><span class="odometer">X,XXX</span></span><img src="img/devices/gentle_laser_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 card-item" id="spectra">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                   <span id="spectra" class="rep-span-device"><span class="odometer">X,XXX</span></span> <img src="img/devices/spectra_logo.png">
                                                </div>
                                             </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-block">
                                    <div class="row no-gutters rep-clinic-row">
                                        <div class="col-lg-6 card-item" id="ActionII">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <span id="ActionII" class="rep-span-device"><span class="odometer">X,XXX</span></span><img src="img/devices/action2_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 card-item" id="eCo2">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <span id="eCo2" class="rep-span-device"><br/><span class="odometer">X,XXX</span></span><img class="dev-04-eco2-re" src="img/devices/eco2_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-block">
                                    <div class="row no-gutters rep-clinic-row">
                                        <div class="col-lg-6 card-item" id="Infini">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <span id="Infini" class="rep-span-device"><span class="odometer">X,XXX</span></span><img src="img/devices/infini_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 card-item" id="ultraformer">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                   <span id="ultraformer" class="rep-span-device"><span class="odometer">X,XXX</span></span> <img src="img/devices/ultraformer_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-block rep-clinic-row">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 card-item" id="artas">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <span id="artas" class="rep-span-device"><span class="odometer">X,XXX</span></span><img class="dev-aratas-clinic" src="img/devices/artas_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 card-item" id="vbeam">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                     <span id="vbeam" class="rep-span-device"><span class="odometer">X,XXX</span></span><img src="img/devices/vbeam_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-block rep-clinic-row">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 card-item" id="enCurve">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <span id="enCurve" class="rep-span-device"><span class="odometer">X,XXX</span></span><img class="dev-12-encurve-re" src="img/devices/encurve_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 card-item" id="picoway">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <span id="picoway" class="rep-span-device"><span class="odometer">X,XXX</span></span><img src="img/devices/picoway_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-block rep-clinic-row">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 card-item" id="lasemd">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                     <span id="lasemd" class="rep-span-device"><span class="odometer">X,XXX</span></span><img class="lase-clinic" src="img/devices/lasemd_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 card-item" id="genius">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                     <span id="genius" class="rep-span-device"><span class="odometer">X,XXX</span></span><img src="img/devices/genius_logo.png">       
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="card-block rep-clinic-row">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 card-item" id="Ulfit">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                     <span id="Ulfit" class="rep-span-device"><span class="odometer">X,XXX</span></span><img class="lase-clinic" src="img/devices/ulfit_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
						<!-- device install count all countries except kuwait -->

						<!-- device install count for country kuwait -->
                        <div class="card clinicbluekuwait" style="display:none">
                            <a data-toggle="collapse" href="#taboneclinic" aria-expanded="true" aria-controls="taboneclinic" class="title-wrapper collapsed">
                                <div class="card-header" role="tab">
                                    <h3>EXPERIENCE :  SHARED</h3>
                                    <h4><img src="img/imdad.knowhow.png" alt="imdad knowhow"></h4>
                                </div>
                            </a>

                            <div id="taboneclinic" class="collapse show" role="tabpanel" aria-expanded="true">
                                <div class="card-block">
                                    <div class="row no-gutters rep-clinic-row">
                                        <div class="col-lg-6 card-item" id="spectra_ku">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                   <span id="spectra_ku" class="rep-span-device"><span class="odometer">X,XXX</span></span> <img src="img/devices/spectra_logo.png">
                                                </div>
 
                                            </div>
                                        </div>
                                        <div class="col-lg-6 card-item" id="ActionII_ku">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <span id="ActionII_ku" class="rep-span-device"><span class="odometer">X,XXX</span></span><img src="img/devices/action2_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								
                                <div class="card-block">
                                    <div class="row no-gutters rep-clinic-row">
                                        <div class="col-lg-6 card-item" id="eCo2_ku">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <span id="eCo2_ku" class="rep-span-device"><span class="odometer">X,XXX</span></span><img class="dev-04-eco2-re" src="img/devices/eco2_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 card-item" id="Infini_ku">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <span id="Infini_ku" class="rep-span-device"><span class="odometer">X,XXX</span></span><img src="img/devices/infini_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="card-block">
                                    <div class="row no-gutters rep-clinic-row">
                                        <div class="col-lg-6 card-item" id="clarity_ku">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <span id="clarity_ku" class="rep-span-device"><span class="odometer">X,XXX</span></span><img src="img/devices/clarity_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 card-item" id="ultraformer_ku">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                   <span id="ultraformer_ku" class="rep-span-device"><span class="odometer">X,XXX</span></span> <img src="img/devices/ultraformer_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								
                                <div class="card-block rep-clinic-row">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 card-item" id="artas_ku">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <span id="artas_ku" class="rep-span-device"><span class="odometer">X,XXX</span></span><img class="dev-aratas-clinic" src="img/devices/artas_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 card-item" id="enCurve_ku">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <span id="enCurve_ku" class="rep-span-device"><span class="odometer">X,XXX</span></span><img class="dev-12-encurve-re" src="img/devices/encurve_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="card-block rep-clinic-row">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 card-item" id="lasemd_ku">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                     <span id="lasemd_ku" class="rep-span-device"><span class="odometer">X,XXX</span></span><img class="lase-clinic" src="img/devices/lasemd_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 card-item" id="picoplus_ku">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <span id="picoplus_ku" class="rep-span-device"><span class="odometer">X,XXX</span></span><img src="img/devices/picoplus_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="card-block rep-clinic-row">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 card-item" id="genius_ku">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                     <span id="genius_ku" class="rep-span-device"><span class="odometer">X,XXX</span></span><img class="lase-clinic" src="img/devices/genius_logo.png">       
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 card-item" id="Ulfit_ku">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                     <span id="Ulfit_ku" class="rep-span-device"><span class="odometer">X,XXX</span></span><img src="img/devices/ulfit_logo.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
						<!-- device install count for country kuwait -->

                        <div class="card blue" >
                            <a data-toggle="collapse" href="#tabone" aria-expanded="true" aria-controls="tabone" class="title-wrapper collapsed">
                                <div class="card-header" role="tab">
                                    <h3>EXPERIENCE :  SHARED</h3>
                                    <h4><img src="img/imdad.knowhow.png" alt="imdad knowhow"></h4>
                                </div>
                            </a>

                            <div id="tabone" class="collapse show" role="tabpanel" aria-expanded="true">
                                <div class="card-block">
                                    <div class="row no-gutters">
                                        <div class="col-lg-4 card-item" id="clients">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Clients</h2>
                                                </div>
                                                <p><span id="clients"><span class="odometer">X,XXX</span></span></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 card-item" id="equipment">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Equipment</h2>
                                                </div>
                                                <p>
                                                    <span id="solutions"><span class="odometer">X,XXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 card-item" id="market_share">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Market share</h2>
                                                </div>
                                                <p>
                                                    <span id="market_share"><span>XX</span></span><sup>%</sup>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card orange">
                            <a data-toggle="collapse" href="#tabtwo" aria-expanded="true" aria-controls="tabtwo" class="title-wrapper collapsed">
                                <div class="card-header" role="tab">
                                    <h3>UP-TIME : MAXIMIZED</h3>
                                    <h4><img src="img/imdad.biomed.png" alt="imdad biomed"></h4>
                                </div>
                            </a>

                            <div id="tabtwo" class="collapse show" role="tabpanel" aria-expanded="true">
                                <div class="card-block">
                                    <div class="row no-gutters">
                                        <div class="col-lg-3 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Repair time < 12h<br/><small>(in warranty)</small></h2>
                                                </div>
                                                <p>
                                                    <span id="avg_repair_time_12h_warranty"><span class="odometer">XX</span><sup>%</sup></span> / <span id="total_repair_calls_warranty"><span>XX</span></span></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Repair time < 12h<br/><small>(in + out warranty)</small></h2>
                                                </div>
                                                <p>
                                                    <span id="avg_repair_time_12h_all"><span>XX</span></span><sup>%</sup> / <span id="total_repair_calls_all"><span>XXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>1st time repair</h2>
                                                </div>
                                                <p>
                                                    <span id="first_time_fix_rate"><span class="odometer">XX</span><sup>%</sup></span> / <span id="ftfr_total_repair_calls_all"><span>XX</span> </span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Up-time</h2>
                                                </div>
                                                <p>
                                                    <span id="up_time"><span>XX</span></span><sup>%</sup>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card red">
                            <a data-toggle="collapse" href="#tabthree" aria-expanded="true" aria-controls="tabthree" class="title-wrapper collapsed">
                                <div class="card-header" role="tab">
                                    <h3>USERS : EMPOWERED</h3>
                                    <h4><img src="img/imdad.clinical.png" alt="imdad clinical"></h4>
                                </div>
                            </a>

                            <div id="tabthree" class="collapse show" role="tabpanel" aria-expanded="true">
                                <div class="card-block">
                                    <div class="row no-gutters">
                                        <div class="col-lg-4 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Certified Doctors</h2>
                                                </div>
                                                <p>
                                                    <span id="doctors_trained"><span class="odometer">X,XXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Certified Nurses</h2>
                                                </div>
                                                <p>
                                                    <span id="nurses_trained"><span class="odometer">X,XXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>e-learning courses</h2>
                                                </div>
                                                <p>
                                                    <span id="e_learning_course_delivered"><span class="odometer">X,XXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card light-blue">
                            <a data-toggle="collapse" href="#tabfour" aria-expanded="true" aria-controls="tabfour" class="title-wrapper collapsed">
                                <div class="card-header" role="tab">
                                    <h3>TREATMENTS : IN DEMAND</h3>
                                    <h4><img src="img/imdad.patients.png" alt="imdad patients"></h4>
                                </div>
                            </a>

                            <div id="tabfour" class="collapse show" role="tabpanel" aria-expanded="true">
                                <div class="card-block">
                                    <div class="row no-gutters">
                                        <div class="col-lg-4 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Websites visitors</h2>
                                                </div>
                                                <p>
                                                    <span id="microsite_visitors"><span class="odometer">XXXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 card-item">
                                            <div class="details">
                                                <div class="tab-title-wrapper">
                                                    <h2>Facebook Likes</h2>
                                                </div>
                                                <p>
                                                    <span id="facebook_likes"><span class="odometer">XXXX</span></span>
                                                </p>
                                            </div>
                                        </div>
                                         <div class="col-lg-4 card-item">
                                            <div class="details">
                                                    <div class="tab-title-wrapper">
                                                <h2>Instagram Followers</h2>
                                                </div>
                                                <p>
                                                    <span id="instagram_likes"><span class="odometer">XXXX</span></span>
                                                </p>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="loading-box" style="display: none;"><img src="<?php echo SITE_URL; ?>/img/loading.gif"></div>
                <div class="loading-box-overlay" style="display: none;"></div>

                <div class="tab-title">
                    <h2>technology: carefully selected</h2>
                </div>
                <?php $json = json_decode(file_get_contents(SITE_URL . "/api/report/device/")); ?>
                <?php 
				//echo SITE_URL . "/api/report/device/"; exit;
				// print_r($json); exit;
				?>
                <div class="tab-block">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs imdad-tab">
                        <?php $tabcounter = 0; ?>
                        <?php foreach ($json as $supplier): ?>
                            <?php //print_r($supplier); ?>
                            <li class="nav-item" id="maintab_<?php echo slugify($supplier->label); ?>">
                                <a class="nav-link <?php echo ($tabcounter++ == 0) ? "active" : ""; ?>" data-toggle="tab" href="#<?php echo slugify($supplier->label); ?>" >
                                    <img class="sup-<?php echo slugify($supplier->label); ?>" src="img/suppliers/<?php echo $supplier->logo; ?>">
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content imdad-tab-content">
                        <?php $tabcounter = 0; ?>
                        <?php foreach ($json as $supplier): ?>
                            <div class="tab-pane <?php echo ($tabcounter++ == 0) ? "active" : ""; ?> container tabpane_<?php echo slugify($supplier->label); ?>" id="<?php echo slugify($supplier->label); ?>">
                                <div class="list_item_checkbox">
                                    <?php foreach ($supplier->devices as $device): ?>
                                        <?php
                                            $hidestyle = '';
                                            if (slugify($device->label) == '16-picoplus' || slugify($device->label) == '06-clarity') {
                                                $hidestyle = 'style="display:none;"';
                                            }
                                        ?>
                                        <div class="form-check form-check-inline" id="device_<?php echo slugify($device->label); ?>" <?php echo $hidestyle;?>>
                                            <input class="form-check-input" type="checkbox" id="<?php echo slugify($device->label); ?>" value="<?php echo slugify($device->id); ?>">
                                            <label class="form-check-label" for="<?php echo slugify($device->label); ?>">
                                                <img class="dev-<?php echo slugify($device->label); ?>" src="img/devices/<?php echo $device->logo; ?>">
                                            </label>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="tab-block">
                    <div class="cl-version">
                        <?php $json_ver = json_decode(file_get_contents(SITE_URL . "/api/report/imdadversion/")); ?>
                        <?php print $json_ver['0']->version; ?><br/>Data update: <?php echo date('d F Y', strtotime($json_ver['0']->updated_date)); ?>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <div id="map-canvas"></div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDN-lnpZximfn66wQcEyqXnUfh01OC54PQ&v=3.31&sensor=false&language=ee"></script>
    <!-- <script src="js/odometer.min.js"></script> -->
    <script src="js/popup.js"></script>
    <script src="js/smoothzoom.js"></script>
    <script src="js/markers.js"></script>
    <script src="js/scripts.js"></script>

</body>
</html>