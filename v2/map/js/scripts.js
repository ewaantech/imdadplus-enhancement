var gmarkers1   = [];
var markers1    = [];
var pointers    = [];
var breadcrumb  = [];
var infowindow  = new google.maps.InfoWindow({content: ''});
var bounds      = new google.maps.LatLngBounds();



var iconBase = '/map/img/markers/';
        var icons = {
          "country": {
            icon: iconBase + 'marker-blue.png'
          },
          city: {
            icon: iconBase + 'marker-blue.png'
          }
        };


var json = JSON.parse($.getJSON({'url': "data.php", 'async': false}).responseText);



/**
 * Function to init map
 */

function initialize() {
    var center = new google.maps.LatLng(41.334830, -81.453479);
    var mapOptions = {
        zoom: 4,
        center: center,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        disableDefaultUI: true

    };
    
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    load_markers('gcc','gcc');
    breadcrumb.push("gcc");    
    fit_bounds_markers(5);
    //goback();


}

goback();


function goback(){
    jQuery("#goback").on("click",function(){

        switch( breadcrumb.length ){
            case 3:
                delete_markers();
                console.log("Country, " , breadcrumb[1]);
                load_previous_markers("country",breadcrumb[1]);
                var zoom = 9;
                if( marker1.data.type == "city"){
                    zoom = 11;
                }
                map.setZoom(zoom)
                fit_bounds_markers(zoom);        
                        
                breadcrumb.pop();
                break;
            case 2:
                delete_markers();
                initialize();
                //console.log(json.report);
                jQuery(".mapview-label").html("Middle East");
                FloatingPopup.populate_popup(json.report.alldevice); 
                // jQuery("#goback").hide();
                breadcrumb = ["gcc"];
                // console.log("Country, " , breadcrumb[0]);
                // load_previous_markers("gcc","gcc");
                // var zoom = 9;
                // if( marker1.data.type == "city"){
                //     zoom = 11;
                // }
                // map.setZoom(zoom)
                // fit_bounds_markers(zoom);                                
                // breadcrumb.pop();
                break;
            default:  
                FloatingPopup.populate_popup(json.report);              
                console.log("Nothign to view from this point backward.");
        }
        console.log("Breadcrumbs",breadcrumb);
    });
}



// Init map
initialize();

// FloatingPopup.populate_popup(json.report.alldevice);
setTimeout(function(){
    FloatingPopup.populate_popup(json.report.alldevice);
},1000);


jQuery(function($){

var link = $("ul.imdad-tab li a");

// On clicking of the links do something.
link.on('click', function(e) {

    e.preventDefault();

    var a = $(this).attr("href");

    $(a).slideToggle('fast');


    //$(a).slideToggle('fast');
    $(".imdad-tab-content .tab-pane").not(a).slideUp('fast');
    
});


});
